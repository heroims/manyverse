// SPDX-FileCopyrightText: 2023 The Manyverse Authors
//
// SPDX-License-Identifier: MPL-2.0

import native from './web-paster.native';
import web from './web-paster.web';
declare var _test: typeof native;
declare var _test: typeof web;
export default web;
